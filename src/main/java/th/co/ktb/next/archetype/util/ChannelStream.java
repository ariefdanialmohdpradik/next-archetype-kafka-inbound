package th.co.ktb.next.archetype.util;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * Kafka Stream
 *
 * Inbound = to read message from kafka topic
 * Outbound = to write message to kafka topic
 *
 * Creating interface that defines method for each stream
 *
 * If you need custom interface, you do it here, else use Sink, Source or Processor built by SCS
 */

public interface ChannelStream {

    String INPUT = "input-stream";
    String INPUT_2 = "input-stream-2";

    @Input(INPUT)
    SubscribableChannel readTopicA();

    @Input(INPUT_2)
    SubscribableChannel readTopicB();
}
