package th.co.ktb.next.archetype.interceptor;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Log4j2
@Component
public class KafkaInboundInterceptor {
    public void intercept(String topicName, Object message) {
        Date date = new Date();
        String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
        String currentTime = new SimpleDateFormat("HH:mm:ss").format(date);
        log.debug("{} {}", topicName, message);
    }
}
