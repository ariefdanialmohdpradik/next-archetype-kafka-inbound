package th.co.ktb.next.archetype.configuration;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;
import th.co.ktb.next.archetype.util.ChannelStream;

@Configuration
@EnableBinding(ChannelStream.class)
public class StreamConfiguration {
}
