package th.co.ktb.next.archetype.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.interceptor.KafkaInboundInterceptor;
import th.co.ktb.next.archetype.model.request.SystemRequestEntity;
import th.co.ktb.next.archetype.model.response.SystemResponseEntity;
import th.co.ktb.next.common.service.BaseService;
/*
* Service layer will contain business logic of the particular API and perform orchestration required.
* This layer implements BaseService interface.
*/
@Service
public class GetMessageTopicA implements BaseService<SystemRequestEntity, SystemResponseEntity> {

    @Value("${spring.cloud.stream.bindings.input-stream.destination}")
    private String topic;

    private KafkaInboundInterceptor kafkaInboundInterceptor;

    public GetMessageTopicA(KafkaInboundInterceptor kafkaInboundInterceptor) {
        this.kafkaInboundInterceptor = kafkaInboundInterceptor;
    }

    @Override
    public SystemResponseEntity execute(SystemRequestEntity systemRequestEntity) {
        //todo: inbound interceptor
        kafkaInboundInterceptor.intercept(topic, systemRequestEntity);
        return new SystemResponseEntity();
    }
}
