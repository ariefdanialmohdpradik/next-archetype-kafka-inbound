package th.co.ktb.next.archetype.listener;

import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import th.co.ktb.next.archetype.model.request.SystemRequestEntity;
import th.co.ktb.next.archetype.service.GetMessageTopicA;
import th.co.ktb.next.archetype.service.GetMessageTopicB;
import th.co.ktb.next.archetype.util.ChannelStream;

/*
 * This is the controller layer of the Micro-service.
 * Description:
 * 1. The controller layer will handle those incoming APIs and pass to its respective service.
 * 2. Each controller will be categorized based on a particular API group.
 */

@Log4j2
@Component
public class SystemListener {

    private GetMessageTopicA getMessageTopicA;
    private GetMessageTopicB getMessageTopicB;

    public SystemListener(GetMessageTopicA getMessageTopicA, GetMessageTopicB getMessageTopicB) {
        this.getMessageTopicA = getMessageTopicA;
        this.getMessageTopicB = getMessageTopicB;
    }

    @StreamListener(ChannelStream.INPUT)
    public void readMessagesTopicA(SystemRequestEntity systemRequestEntity) {
        getMessageTopicA.execute(systemRequestEntity);
    }

    @StreamListener(ChannelStream.INPUT_2)
    public void readMessagesTopicB(SystemRequestEntity systemRequestEntity) {
        getMessageTopicB.execute(systemRequestEntity);
    }
}
